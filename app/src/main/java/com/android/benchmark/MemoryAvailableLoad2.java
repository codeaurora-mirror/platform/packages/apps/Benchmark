/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.benchmark;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;

import java.io.FileInputStream;
import java.io.FileOutputStream;


public class MemoryAvailableLoad2 extends Activity {
    int mTargetPlainMemory;
    int mTargetBitmaps;
    int mTargetTextures;
    int mTargetCompute;

    native long nMemTestMalloc(int size);
    native void nMemTestFree(long p);

    static {
        System.loadLibrary("nativeMemory");
    }

    long mLoadPtr = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memory_available_load1);
        android.util.Log.e("bench", "mal2 onCreate");
    }

    @Override
    protected void onPause() {
        super.onPause();
        android.util.Log.e("bench", "mal2 onPause");
    }

    @Override
    protected void onStart() {
        super.onStart();
        android.util.Log.e("bench", "mal2 onStart");
    }

    protected void onResume() {
        String checkFilename = "MemoryAvailableLoad2.check";
        super.onResume();
        android.util.Log.e("bench", "mal2 onResume ");

        Intent i = getIntent();
        int reset = i.getIntExtra("reset", 0);
        mTargetPlainMemory = i.getIntExtra("plain", 0);
        mTargetBitmaps = i.getIntExtra("bitmaps", 0);
        mTargetTextures = i.getIntExtra("textures", 0);
        mTargetCompute = i.getIntExtra("compute", 0);

        android.util.Log.e("bench", "mal2 plain " + mTargetPlainMemory);

        if (mLoadPtr != 0) {
            nMemTestFree(mLoadPtr);
            mLoadPtr = 0;
        }

        if ((reset == 0) && checkFileCheck(checkFilename)) {
            // Failed to delete the file, assume crashed during alloc
            finishTest(false);
        } else {
            if (checkFileCreate(checkFilename)) {
                mLoadPtr = nMemTestMalloc(mTargetPlainMemory * 1024 * 1024);
            }
        }
        checkFileDelete(checkFilename);

        android.util.Log.e("bench", "mal2 alloc  " );


        final MemoryAvailableLoad2 mal = this;
        final Handler handler = new Handler();
        int delay = 5;

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                finishTest(true);
            }
        }, (1000 * delay));

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        android.util.Log.e("bench", "mal2 onRestart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        android.util.Log.e("bench", "mal2 onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        android.util.Log.e("bench", "mal2 onDestroy");

        if (mLoadPtr != 0) {
            nMemTestFree(mLoadPtr);
            mLoadPtr = 0;
        }
    }

    @Override
    public void onTrimMemory(int level) {

        android.util.Log.e("bench", "mal2 on trim " + level);

        if (mLoadPtr != 0) {
            nMemTestFree(mLoadPtr);
            mLoadPtr = 0;
        }
        finishTest(false);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        android.util.Log.e("bench", "mal2 onSaveInstanceState");
        //savedInstanceState.putBoolean("wasKilled", mFinished);

    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        android.util.Log.e("bench", "mal2 onRestoreInstanceState");
        //mFinished = savedInstanceState.getBoolean("wasKilled");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_memory_available_load1, menu);
        return true;
    }

    private void finishTest(boolean ok) {
        android.util.Log.e("bench", "mal2 finishTest ok = " + ok);
        if (ok) {
            Intent intent = new Intent();
            intent.putExtra("plain", mTargetPlainMemory);
            intent.putExtra("bitmaps", mTargetBitmaps);
            intent.putExtra("textures", mTargetTextures);
            intent.putExtra("compute", mTargetCompute);
            setResult(RESULT_OK, intent);
        } else {
            setResult(RESULT_CANCELED);
        }
        finish();
    }

    private boolean checkFileCreate(String filename) {
        String string = "file ok";
        FileOutputStream outputStream;

        try {
            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(string.getBytes());
            outputStream.flush();
            outputStream.close();
            return true;
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return false;
    }

    private boolean checkFileCheck(String filename) {
        FileInputStream stream;

        try {
            stream = openFileInput(filename);
            stream.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean checkFileDelete(String filename) {
        return deleteFile(filename);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
